package com.user.servicesimpl;


import com.user.dto.JwtAuthenticationResponse;
import com.user.dto.SignInRequest;
import com.user.dto.SignUpRequest;
import com.user.entities.User;
import com.user.services.JWTTokenClient;
import com.user.services.UserService;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
@Data
public class UserServiceImpl implements UserService {

  private final JWTTokenClient jwtTokenClient;

  @Override
  public User signUp(SignUpRequest signUpRequest) {
    return jwtTokenClient.signup(signUpRequest).getBody();
  }

  @Override
  public JwtAuthenticationResponse signIn(SignInRequest signInRequest) {
    return jwtTokenClient.signIn(signInRequest).getBody();
  }

  @Override
  public JwtAuthenticationResponse refreshToken(SignInRequest signInRequest) {
    return jwtTokenClient.getToken(signInRequest).getBody();
  }
}
