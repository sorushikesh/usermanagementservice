package com.user.services;

import com.user.dto.JwtAuthenticationResponse;
import com.user.dto.SignInRequest;
import com.user.dto.SignUpRequest;
import com.user.entities.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "JWT-TOKEN-SERVICE")
public interface JWTTokenClient {

  @PostMapping("/api/v1/auth/signup")
  ResponseEntity<User> signup(@RequestBody SignUpRequest signUpRequest);

  @PostMapping("/api/v1/auth/signin")
  ResponseEntity<JwtAuthenticationResponse> signIn(@RequestBody SignInRequest signInRequest);

  @PostMapping("/api/v1/auth/token")
  ResponseEntity<JwtAuthenticationResponse> getToken(@RequestBody SignInRequest signInRequest);
}
