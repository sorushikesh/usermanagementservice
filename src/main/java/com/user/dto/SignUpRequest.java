package com.user.dto;

import com.user.entities.Role;
import lombok.Data;

@Data
public class SignUpRequest {

  private String username;
  private String password;
  private Role role;
}
