package com.user.dto;

import lombok.Data;

@Data
public class JwtAuthenticationResponse {

  private String username;
  private String token;
}
