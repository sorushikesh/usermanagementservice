package com.user.controller;

import com.user.dto.JwtAuthenticationResponse;
import com.user.dto.SignInRequest;
import com.user.dto.SignUpRequest;
import com.user.entities.User;
import com.user.services.UserService;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Data
@RestController
@RequestMapping("/api/user")
public class UserController {

  private final UserService userService;

  @PostMapping("/signup")
  public ResponseEntity<User> signup(@RequestBody SignUpRequest signUpRequest) {
    return ResponseEntity.ok(userService.signUp(signUpRequest));
  }

  @PostMapping("/signin")
  public ResponseEntity<JwtAuthenticationResponse> signIn(@RequestBody SignInRequest signInRequest) {
    return ResponseEntity.ok(userService.signIn(signInRequest));
  }
}
